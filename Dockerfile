FROM node:8-alpine

# Sytem packages
RUN apk add yarn git

# Files
USER node
COPY app /var/build

# Permissions
USER root
COPY docker/app/install.sh /usr/local/bin/install
RUN chown -R node:node /var/build \
    && mkdir -p /var/app \
    && chown -R node:node /var/app \
    && chmod +x /usr/local/bin/install

RUN chown -R node:node /var/build

# Build
USER node
WORKDIR /var/build
ARG ENV
RUN if [ "$ENV" != "" ]; then \
        export NODE_ENV=$ENV; \
    fi \
    && install

# Environment
ENV HOME /var/app
WORKDIR $HOME
ENV NODE_ENV production

CMD ["/usr/bin/yarn", "start:prod"]