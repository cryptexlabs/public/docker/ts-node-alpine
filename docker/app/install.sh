yarn --cwd /var/build install  \
    && yarn --cwd /var/build build \
    && cp -rf /var/build/dist/. /var/app/dist \
    && cp -f /var/build/package.json /var/app/package.json

if [ "${NODE_ENV}" != "development" ]; then
    echo "Removing  /var/build/*"
    rm -rf /var/build/*;
fi;